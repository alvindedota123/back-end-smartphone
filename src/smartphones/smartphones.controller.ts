import { Controller, Post, Body, Get, Param, ParseIntPipe, Delete, Patch } from '@nestjs/common';
import { CreateSmartphoneDto } from './dto/create-smartphone.dto';
import { SmartphonesService } from './smartphones.service';
import { Smartphone } from './smartphones.entity';
import { UpdateSmartphoneDto } from './dto/update-smartphone.dto';

@Controller('smartphones')
export class SmartphonesController {
    
    constructor(private smartphoneService: SmartphonesService) {}

    @Get()
    getSmartphones(): Promise<Smartphone[]> {
        return this.smartphoneService.getSmartphones()
    }

    @Get(':id')
    getSmartphone(@Param('id', ParseIntPipe) id: number): Promise<Smartphone> {
        return this.smartphoneService.getSmartphone(id)
    }


    @Post()
    createSmartphone(@Body() newSmartphone: CreateSmartphoneDto): Promise<Smartphone> {
        return this.smartphoneService.createSmartphone(newSmartphone)
    }

    @Delete(':id')
    deleteSmartphone(@Param('id', ParseIntPipe) id: number) {
        return this.smartphoneService.deleteSmartphone(id)
    }

    @Patch(':id')
    updateSmartphone(@Param('id', ParseIntPipe) id:number, @Body() smartphone: UpdateSmartphoneDto){
        return this.smartphoneService.updateSmartphone(id, smartphone)
    }

}

