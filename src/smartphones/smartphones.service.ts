import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm'
import { Smartphone } from './smartphones.entity';
import {Repository} from 'typeorm'
import {CreateSmartphoneDto} from './dto/create-smartphone.dto'
import {UpdateSmartphoneDto} from './dto/update-smartphone.dto'

@Injectable()
export class SmartphonesService {
    constructor(@InjectRepository(Smartphone) private smartphoneRepository: Repository<Smartphone>) {}

    createSmartphone(smartphone: CreateSmartphoneDto) {
        const newSmartphone = this.smartphoneRepository.create(smartphone)
        return this.smartphoneRepository.save(newSmartphone)
    }

    getSmartphones() {
        return this.smartphoneRepository.find()
    }

    getSmartphone(id: number) {
        return this.smartphoneRepository.findOne({
            where: {
                id
            }
        })
    }

    deleteSmartphone(id: number) {
        return this.smartphoneRepository.delete({ id })
    }

    updateSmartphone(id: number, smartphone: UpdateSmartphoneDto) {
        return this.smartphoneRepository.update({id}, smartphone)
    }

}
