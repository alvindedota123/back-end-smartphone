import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn} from 'typeorm'

@Entity({ name: "smartphones"})
export class Smartphone {
    @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255 })
  nombre: string;

  @Column({ length: 255 })
  modelo: string;

  @Column('float')
  precio_referencial: number;

  @Column('float')
  precio_venta: number;

  @Column()
  ano_modelo: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}