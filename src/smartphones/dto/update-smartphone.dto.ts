export class UpdateSmartphoneDto {
    nombre?: string
    modelo?: string 
    precio_referencial?: number
    precio_venta?: number
    ano_modelo?: number
}